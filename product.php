<html>
<head>
	<title>Our products</title>
	<?php include "head.php" ?>
</head>
<body>
	<?php include "nav.php" ?>
	<main>
        <section class='product-nav-wrapper'>
            <div id='product-nav-konten'>
                <div id='product-nav-body' class='owl-carousel'>
                    <div class='product-nav-item'>
                        <img src="assets/images/study.png">
                        <h4>SHERMAN</h4>
                    </div>
                    <div class='product-nav-item'>
                        <img src="assets/images/news.png">
                        <h4>ADENS</h4>
                    </div>
                    <div class='product-nav-item'>
                        <img src="assets/images/screen.png">
                        <h4>BONITA</h4>
                    </div>
                    <div class='product-nav-item'>
                        <img src="assets/images/keyman.png">
                        <h4>KEYMAN</h4>
                    </div>
                    <div class='product-nav-item'>
                        <img src="assets/images/connections.png">
                        <h4>KONGURITY</h4>
                    </div>
                </div>
            </div>
        </section>
        <div id='product-nav-button-wrapper'>
                <div class='product-nav-button'>
                    <!-- <img src="assets/images/up.png"> -->
                </div>
        </div>
        <section class='product-outer-wrapper product-1'>
            <div class='container'>
                <div class='product-konten'>
                    <h2>Lorem Ipsum</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                </div>
                <div class='product-image'>
                    <img src="assets/images/produk.png">
                </div>
            </div>
        </section>
        <section class='product-outer-wrapper'>
            <div class='container'>
                <div class='product-konten'>
                    <h2>Lorem Ipsum</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                </div>
                <div class='product-image'>
                    <img src="assets/images/produk.png">
                </div>
            </div>
        </section>
        <section class='product-outer-wrapper'>
            <div class='container'>
                <div class='product-konten'>
                    <h2>Lorem Ipsum</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                </div>
                <div class='product-image'>
                    <img src="assets/images/produk.png">
                </div>
            </div>
        </section>
        <section class='product-outer-wrapper'>
            <div class='container'>
                <div class='product-konten'>
                    <h2>Lorem Ipsum</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                </div>
                <div class='product-image'>
                    <img src="assets/images/produk.png">
                </div>
            </div>
        </section>
        <section class='product-outer-wrapper'>
            <div class='container'>
                <div class='product-konten'>
                    <h2>Lorem Ipsum</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                </div>
                <div class='product-image'>
                    <img src="assets/images/produk.png">
                </div>
            </div>
        </section>
	</main>
	<section class='recent-blog'>
                <div class='container'>
                    <h1>Recent Blog Post</h1>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-1b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-2b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-3b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-4b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                </div>
            </section>
	<?php include "footer.php" ?>
</body>
</html>