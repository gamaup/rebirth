<html>
<head>
	<title>Contact</title>
	<?php include "head.php" ?>
</head>
<body>
	<?php include "nav.php" ?>
	<main>
        <?php include 'map.php'; ?>
        <section class="contact">
            <div class="container">
                <div id="contact-kiri">
                    
                    <form action="#" method="post" id="form">
                        <label>Your Name</label><br>
                        <input type="text" name="nama"><br>
                        <label>Your Email</label><br>
                        <input type="text" name="email"><br>
                        <label>Subject</label><br>
                        <input type="text" name="subject"><br>
                        <label>Message</label><br>
                        <textarea spellcheck="false" name="message"></textarea>
                        <input type="submit" value="Send">
                    </form>
                </div>
                <div id="contact-kanan">
                    <h2>Contact Us</h2>
                    <h3>Address</h3>
                    <p>Jl. Lorem Ipsum No.00 Yogyakarta</p>
                    <h3>Contact Details</h3>
                    <p>+628xxxxxx <br>
                    customerservice@inncomedia.com <br>
                    @inncomedia</p>
                    <h3>Social Media</h3>
                    <div class="social">
                        <img src="assets/images/facebook.png" alt="">
                        <div class="socialbox">
                            <p class="text"><img src="assets/images/facebook.png"></p>
                        </div>
                    </div>
                    <div class="social">
                        <img src="assets/images/twitter.png" alt="">
                        <div class="socialbox">
                            <p class="text"><img src="assets/images/twitter.png"></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	</main>
	<section class='recent-blog'>
                <div class='container'>
                    <h1>Recent Blog Post</h1>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-1b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-2b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-3b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-4b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                </div>
            </section>
	<?php include "footer.php" ?>
</body>
</html>