<nav>
    <div class='container'>
        <img src='assets/images/logo.png' class='nav-logo'/>
        <ul class='nav-group'>
            <li class='active'><a href='index.php'>Home</a></li>
            <li><a href='about-us.php'>About Us</a></li>
            <li><a href='#'>What We Do</a></li>
            <li><a href='portofolio-view.php'>Portofolio</a></li>
            <li><a href='product.php'>Product</a></li>
            <li><a href='clients.php'>Clients</a></li>
            <li><a href='blog.php'>Blog</a></li>
            <li><a href='contact.php'>Contact</a></li>
        </ul>
        <div class='push-nav-toggle'>
            <span></span>
        </div>
    </div>
</nav>
<div class='push-nav'>
    <ul class='push-nav-group'>
        <li class='active'><a href='index.php'>Home</a></li>
        <li><a href='about-us.php'>About Us</a></li>
        <li><a href='#'>What We Do</a></li>
        <li><a href='portofolio-view.php'>Portofolio</a></li>
        <li><a href='product.php'>Product</a></li>
        <li><a href='#'>Clients</a></li>
        <li><a href='blog.php'>Blog</a></li>
        <li><a href='contact.php'>Contact</a></li>
    </ul>
</div>