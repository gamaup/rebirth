<!DOCTYPE html>
<html>
    <head>
        <title>Blog | Inncomedia</title>
        <?php include 'head.php'; ?>
    </head>
    <body>
        <?php include 'nav.php'; ?>
        <main>
            <section class='featured-image' style='background-image:url(assets/images/sample-3.jpg)'>
                <div class='overlay'>
                    <div class='post-info'>
                        <h2 class='title small-container'>Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique.</h2>
                        <div class='post-detail small-container'>
                            <span class='post-date'>Thu, 25 Nov 2014</span><span class='post-author'>Gama Unggul Priambada</span>
                        </div>
                    </div>
                </div>
            </section>
            <section class='blog-content small-container'>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin semper, ante vitae sollicitudin posuere, metus quam iaculis nibh, vitae scelerisque nunc massa eget pede. Sed velit urna, interdum vel, ultricies vel, faucibus at, quam. Donec elit est, consectetuer eget, consequat quis, tempus quis, wisi. In in nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>
                <p>Donec ullamcorper fringilla eros. Fusce in sapien eu purus dapibus commodo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                <img class='float-left' src='assets/images/sample-4.jpg'/>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.
Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.</p>
                <p>Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla. Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</p>
            </section>
            <section class='start-project'>
                <a href='#'>
                    <h1><span>Let's Start Your Project Now!</span></h1>
                </a>
            </section>
            <section class='recent-blog'>
                <div class='container'>
                    <h1>Recent Blog Post</h1>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-1b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-2b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-3b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-4b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                </div>
            </section>
            <?php include 'footer.php' ?>
        </main>
    </body>
</html>