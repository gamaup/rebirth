<!DOCTYPE html>
<html>
    <head>
        <title>Blog | Inncomedia</title>
        <?php include 'head.php'; ?>
    </head>
    <body>
        <?php include 'nav.php'; ?>
        <main>
            <h1 class='page-title'>Blog</h1>
            <section class='blog-wrap small-container'>
                <div class='blog-container'>
                    <div class='blog-image' style='background-image:url(assets/images/sample-1b.jpg)'>
                    </div>
                    <div class='blog-detail'>
                        <a href='blog-view.php'><h3 class='blog-title'>Pellentesque habitant morbi tristique senectus et netus</h3></a>
                        <h5 class='post-date'>Thu, 25 Nov 2014</h5>
                        <p class='blog-summary'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        <a class='readmore' href='blog-view.php'>Read More <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
                <div class='blog-container'>
                    <div class='blog-image' style='background-image:url(assets/images/sample-2b.jpg)'>
                    </div>
                    <div class='blog-detail'>
                        <a href='blog-view.php'><h3 class='blog-title'>Pellentesque habitant morbi tristique senectus et netus</h3></a>
                        <h5 class='post-date'>Thu, 25 Nov 2014</h5>
                        <p class='blog-summary'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        <a class='readmore' href='blog-view.php'>Read More <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
                <div class='blog-container'>
                    <div class='blog-image' style='background-image:url(assets/images/sample-3b.jpg)'>
                    </div>
                    <div class='blog-detail'>
                        <a href='blog-view.php'><h3 class='blog-title'>Pellentesque habitant morbi tristique senectus et netus</h3></a>
                        <h5 class='post-date'>Thu, 25 Nov 2014</h5>
                        <p class='blog-summary'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        <a class='readmore' href='blog-view.php'>Read More <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
                <div class='blog-container'>
                    <div class='blog-image' style='background-image:url(assets/images/sample-4b.jpg)'>
                    </div>
                    <div class='blog-detail'>
                        <a href='blog-view.php'><h3 class='blog-title'>Pellentesque habitant morbi tristique senectus et netus</h3></a>
                        <h5 class='post-date'>Thu, 25 Nov 2014</h5>
                        <p class='blog-summary'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        <a class='readmore' href='blog-view.php'>Read More <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
                <div class='blog-container'>
                    <div class='blog-image' style='background-image:url(assets/images/sample-1b.jpg)'>
                    </div>
                    <div class='blog-detail'>
                        <a href='blog-view.php'><h3 class='blog-title'>Pellentesque habitant morbi tristique senectus et netus</h3></a>
                        <h5 class='post-date'>Thu, 25 Nov 2014</h5>
                        <p class='blog-summary'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        <a class='readmore' href='blog-view.php'>Read More <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
                <div class='blog-container'>
                    <div class='blog-image' style='background-image:url(assets/images/sample-2b.jpg)'>
                    </div>
                    <div class='blog-detail'>
                        <a href='blog-view.php'><h3 class='blog-title'>Pellentesque habitant morbi tristique senectus et netus</h3></a>
                        <h5 class='post-date'>Thu, 25 Nov 2014</h5>
                        <p class='blog-summary'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        <a class='readmore' href='blog-view.php'>Read More <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </section>
            <section class='paging small-container'>
                Page: 
                <a href='#' class='active'>1</a>
                <a href='#'>2</a>
                <a href='#'>3</a>
                <a href='#'>4</a>                
            </section>
            <section class='start-project'>
                <a href='#'>
                    <h1><span>Let's Start Your Project Now!</span></h1>
                </a>
            </section>
            <?php include 'footer.php' ?>
        </main>
    </body>
</html>