<!DOCTYPE html>
<html>
    <head>
        <title>Inncomedia</title>
        <?php include 'head.php'; ?>
    </head>
    <body>
        <?php include 'nav.php'; ?>
        <main>
            <section class='slider'>
            </section>
            <section class='whatwedo'>
                <p class="container">IT memiliki kekuatan untuk membawa bisnis Anda ketingakat selanjutnya. Maka penting bagi Anda untuk memilih IT Support yang tepat. Kami siap memberikan solusi dan layanan terbaik kami untuk Anda.</p>
                <div class='container'>
                    <div id="whatwedo-slide" class="owl-carousel">
                        <div class='whatwedo-item'>
                            <img src='assets/images/svg/website.svg' alt='Website Development'/>
                            <h3>Website Development</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        </div>
                        <div class='whatwedo-item'>
                            <img src='assets/images/svg/software.svg' alt='Software Development'/>
                            <h3>Software Development</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        </div>
                        <div class='whatwedo-item'>
                            <img src='assets/images/svg/mobile.svg' alt='Mobile App Development'/>
                            <h3>Mobile App Development</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        </div>
                        <div class='whatwedo-item'>
                            <img src='assets/images/svg/multimedia.svg' alt='Multimedia'/>
                            <h3>Multimedia</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        </div>
                        <div class='whatwedo-item'>
                            <img src='assets/images/svg/consultant.svg' alt='IT Support & Consultant'/>
                            <h3>IT Support &amp; Consultant</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        </div>
                        <div class='whatwedo-item'>
                            <img src='assets/images/svg/network.svg' alt='Network Infrastruture'/>
                            <h3>Network Infrastructure</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        </div>
                        <div class='whatwedo-item'>
                            <img src='assets/images/svg/hardware.svg' alt='Hardware Procurement'/>
                            <h3>Hardware Procurement</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        </div>
                        <div class='whatwedo-item'>
                            <img src='assets/images/svg/hosting.svg' alt='Hosting'/>
                            <h3>Hosting</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                        </div>
                    </div>
                </div>
                <div class='whatwedo-prev'><i class="fa fa-angle-left"></i>
                </div>
                <div class='whatwedo-next'><i class="fa fa-angle-right"></i>
                </div>
                
                <h2><a href='#'>View All Our Service Details</a></h2>
            </section>
            <section class='latest-work'>
                <div class='latest-work-title'>
                    <h1>Our Latest Work</h1>
                    <a href='#'>View All</a>
                </div>
                <div class='latest-work-wrapper'>
                    <div class='latest-work-item' style='background-image:url(assets/images/sample-1b.jpg)'>
                        <div class='overlay'>
                            <h4><a href='#'>View Details</a></h4>
                        </div>
                    </div>
                    <div class='latest-work-item' style='background-image:url(assets/images/sample-2b.jpg)'>
                        <div class='overlay'>
                            <h4><a href='#'>View Details</a></h4>
                        </div>
                    </div>
                    <div class='latest-work-item' style='background-image:url(assets/images/sample-3b.jpg)'>
                        <div class='overlay'>
                            <h4><a href='#'>View Details</a></h4>
                        </div>
                    </div>
                    <div class='latest-work-item' style='background-image:url(assets/images/sample-4b.jpg)'>
                        <div class='overlay'>
                            <h4><a href='#'>View Details</a></h4>
                        </div>
                    </div>
                </div>
            </section>
<!--            nambahin sesuatu-->
            <section class='testimoni container'>
                <div id="testimoni-slide" class="owl-carousel">
                    <div class='testimoni-item'>
                        <p>"Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci."</p>
                        <h3>Daenerys Targaryen</h3>
                        <h5>Mother of Dragons</h5>
                    </div>
                    <div class='testimoni-item'>
                        <p>"Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy."</p>
                        <h3>Jon Snow</h3>
                        <h5>Watcher on The Wall</h5>
                    </div>
                    <div class='testimoni-item'>
                        <p>"Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin semper, ante vitae sollicitudin posuere, metus quam iaculis nibh, vitae scelerisque nunc massa eget pede. Sed velit urna, interdum vel, ultricies vel, faucibus at, quam."</p>
                        <h3>Tyrion Lannister</h3>
                        <h5>The Imp</h5>
                    </div>
                </div>
            </section>
            <section class='start-project'>
                <a href='#'>
                    <h1><span>Let's Start Your Project Now!</span></h1>
                </a>
            </section>
            <section class='recent-blog'>
                <div class='container'>
                    <h1>Recent Blog Post</h1>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-1b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-2b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-3b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-4b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                </div>
            </section>
            <?php include 'footer.php' ?>
        </main>
    </body>
</html>