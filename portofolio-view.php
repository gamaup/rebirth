<!DOCTYPE html>
<html>
    <head>
        <title>Portofolio | Inncomedia</title>
        <?php include 'head.php'; ?>
    </head>
    <body>
        <?php include 'nav.php'; ?>
        <main>
            <section class='portofolio-landing' style='background-image:url(assets/images/sample-1.jpg)'>
                <div>
                    <h1 class='project-title container'>
                        Lorem Ipsum Website Development
                    </h1>
                    <div class='project-client container'>
                        <h5>&#8212; client &#8212;</h5>
                        <h4>CV. Lorem Ipsum</h4>
                    </div>
                    <div class='project-work container'>
                        <h5>&#8212; works &#8212;</h5>
                        <h4>Website, Cashier System</h4>
                    </div>
                </div>
            </section>
            <section class='client-profile small-container'>
                <img class='client-logo' src='assets/images/logo.png'/>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                <p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</p>
            </section>
            <section class='project-snapshot'>
                <div id="project-snapshot" class="owl-carousel">
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-1b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-2b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-3b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-4b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-1b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-2b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-3b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-4b.jpg)'>
                    </div>
                </div>
            </section>
            <section class='project-story small-container'>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</p>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                <p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</p>
            </section>
            <section class='start-project'>
                <a href='#'>
                    <h1><span>Let's Start Making Your Own Project</span></h1>
                </a>
            </section>
            <?php include 'footer.php' ?>
        </main>
    </body>
</html>