<!DOCTYPE html>
<html>
    <head>
        <title>What We Do | Inncomedia</title>
        <?php include 'head.php'; ?>
    </head>
    <body>
        <?php include 'nav.php'; ?>
        <main>
            <section class='whatwedo-wrapper small-container'>
                <div class='whatwedo-container'>
                    <div class='whatwedo-content' style='background-image:url(assets/images/wwd-website.png)'>
                        <div class='whatwedo-info'>
                            <h3 class='title'><span>Website Development</span></h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                            <div class='detail'>
                                <a href='#'><i class="fa fa-chevron-down"></i><span>View Detail</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php include 'footer.php' ?>
        </main>
    </body>
</html>