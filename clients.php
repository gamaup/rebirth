<html>
<head>
	<title>OUR CLIENTS</title>
	<?php include "head.php" ?>
</head>
<body>
    <main>
    	<?php include "nav.php" ?>
    	<section class='clients-wrapper'>
            <div class='tag-clients'>
                <h1>
                    Clients We Have Worked With
                </h1>
            </div>
            <div class='container'>
                <div class='container-view-clients'>
                    <ul>
                        <li>
                            <img src="assets/images/clients/gilapasta.png">
                            <label>Gilapasta - Restoran Pasta Yogyakarta</label>
                        </li>
                        <li>
                            <img src="assets/images/clients/gmpro.png">
                            <label>GM Production - Sound System & EO Yogyakarta</label>
                        </li>
                        <li>
                            <img src="assets/images/clients/hmtl.png">
                            <label>Himpunan Mahasiswa Teknik Lingkungan UII</label>
                        </li>
                        <li>
                            <img src="assets/images/clients/kosmikgath.png">
                            <label>Gathering Komunitas Musik Informatika UII</label>
                        </li>
                        <li>
                            <img src="assets/images/clients/majesty.png">
                            <label>Majesty Beauty and Salon</label>
                        </li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
            <div class='testi'>
                <h1 >
                    Testimonial
                </h1>
            </div>
                <section class='testimoni container'>
                    <div id="testimoni-slide" class="owl-carousel">
                        <div class='testimoni-item'>
                            <p>"Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci."</p>
                            <h3>Daenerys Targaryen</h3>
                            <h5>Mother of Dragons</h5>
                        </div>
                        <div class='testimoni-item'>
                            <p>"Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy."</p>
                            <h3>Jon Snow</h3>
                            <h5>Watcher on The Wall</h5>
                        </div>
                        <div class='testimoni-item'>
                            <p>"Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin semper, ante vitae sollicitudin posuere, metus quam iaculis nibh, vitae scelerisque nunc massa eget pede. Sed velit urna, interdum vel, ultricies vel, faucibus at, quam."</p>
                            <h3>Tyrion Lannister</h3>
                            <h5>The Imp</h5>
                        </div>
                    </div>
                </section>
        </section>
    </main>
	<section class='recent-blog'>
                <div class='container'>
                    <h1>Recent Blog Post</h1>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-1b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-2b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-3b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-4b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                </div>
            </section>
	<?php include "footer.php" ?>
</body>
</html>