<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
        <link rel="stylesheet" type="text/css" href="assets/fa/css/font-awesome.min.css"/>
        <link rel='stylesheet' type='text/css' href='assets/css/style.css'/>
        <link rel='stylesheet' type='text/css' href='assets/plugins/owl-carousel/owl.carousel.css'/>
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css">
        <script src='//api.tiles.mapbox.com/mapbox.js/v1.6.0/mapbox.js'></script>
        <link href='//api.tiles.mapbox.com/mapbox.js/v1.6.0/mapbox.css' rel='stylesheet' />
        <link rel='stylesheet' type='text/css' href='assets/plugins/owl-carousel/owl.theme.css'/>
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
        <script src="assets/js/script.js"></script>
        <script src="assets/fancybox/jquery.fancybox.js"></script>
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png">