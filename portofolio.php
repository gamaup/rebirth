<!DOCTYPE html>
<html>
    <head>
        <title>Portofolio | Inncomedia</title>
        <?php include 'head.php'; ?>
    </head>
    <body>
        <?php include 'nav.php'; ?>
        <main>
            <section class='portofolio-wrap'>
                

        <div class="gallery">

            <div class="filter">
                <div>
                    <span>Filter Portofolio by Categories: </span>
                    <a href="#" class="sortLink selected" data-category="all">All</a>
                    <a href="#" class="sortLink" data-category="city">City</a>
                    <a href="#" class="sortLink" data-category="lake">Lake</a>
                    <a href="#" class="sortLink" data-category="park">Park</a>
                    <a href="#" class="sortLink" data-category="wired">Wired</a>
                    <div class="clear_floats"></div>
                </div>
            </div>
<div class="container">
            <div class="photos">
                
                <div class="thumbnail_wrap">

                    <a href="photos/city-1.jpg" class="thumbnail" data-categories="city" title="City Photo 1 Caption">
                        <img src="photos/city-1.jpg" alt="City 1">
                    </a>

                    <a href="photos/city-2.jpg" class="thumbnail" data-categories="city" title="City Photo 2 Caption">
                        <img src="photos/city-2.jpg" alt="City 2">
                    </a>

                    <a href="photos/city-3.jpg" class="thumbnail" data-categories="city" title="City Photo 3 Caption">
                        <img src="photos/city-3.jpg" alt="City 3">
                    </a>

                    <a href="photos/city-4.jpg" class="thumbnail" data-categories="city" title="City Photo 4 Caption">
                        <img src="photos/city-4.jpg" alt="City 4">
                    </a>

                    <a href="photos/city-5.jpg" class="thumbnail" data-categories="city" title="City Photo 5 Caption">
                        <img src="photos/city-5.jpg" alt="City 5">
                    </a>

                    <a href="photos/city-6.jpg" class="thumbnail" data-categories="city" title="City Photo 6 Caption">
                        <img src="photos/city-6.jpg" alt="City 6">
                    </a>

                    <a href="photos/city-7.jpg" class="thumbnail" data-categories="city" title="City Photo 7 Caption">
                        <img src="photos/city-7.jpg" alt="City 7">
                    </a>

                    <a href="photos/city-8.jpg" class="thumbnail" data-categories="city" title="City Photo 8 Caption">
                        <img src="photos/city-8.jpg" alt="City 8">
                    </a>

                    <a href="photos/city-9.jpg" class="thumbnail" data-categories="city" title="City Photo 9 Caption">
                        <img src="photos/city-9.jpg" alt="City 9">
                    </a>

                    <a href="photos/city-10.jpg" class="thumbnail" data-categories="city" title="City Photo 10 Caption">
                        <img src="photos/city-10.jpg" alt="City 10">
                    </a>

                    <a href="photos/city-11.jpg" class="thumbnail" data-categories="city" title="City Photo 11 Caption">
                        <img src="photos/city-11.jpg" alt="City 11">
                    </a>

                    <a href="photos/lake-1.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 1 Caption">
                        <img src="photos/lake-1.jpg" alt="Lake 1">
                    </a>

                    <a href="photos/lake-2.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 2 Caption">
                        <img src="photos/lake-2.jpg" alt="Lake 2">
                    </a>

                    <a href="photos/lake-3.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 3 Caption">
                        <img src="photos/lake-3.jpg" alt="Lake 3">
                    </a>

                    <a href="photos/lake-4.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 4 Caption">
                        <img src="photos/lake-4.jpg" alt="Lake 4">
                    </a>

                    <a href="photos/lake-5.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 5 Caption">
                        <img src="photos/lake-5.jpg" alt="Lake 5">
                    </a>

                    <a href="photos/lake-6.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 6 Caption">
                        <img src="photos/lake-6.jpg" alt="Lake 6">
                    </a>

                    <a href="photos/lake-7.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 7 Caption">
                        <img src="photos/lake-7.jpg" alt="Lake 7">
                    </a>

                    <a href="photos/lake-8.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 8 Caption">
                        <img src="photos/lake-8.jpg" alt="Lake 8">
                    </a>

                    <a href="photos/lake-9.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 9 Caption">
                        <img src="photos/lake-9.jpg" alt="Lake 9">
                    </a>

                    <a href="photos/lake-10.jpg" class="thumbnail" data-categories="lake" title="Lake Photo 10 Caption">
                        <img src="photos/lake-10.jpg" alt="Lake 10">
                    </a>

                    <a href="photos/park-1.jpg" class="thumbnail" data-categories="park" title="Park Photo 1 Caption">
                        <img src="photos/park-1.jpg" alt="Park 1">
                    </a>

                    <a href="photos/park-2.jpg" class="thumbnail" data-categories="park" title="Park Photo 2 Caption">
                        <img src="photos/park-2.jpg" alt="Park 2">
                    </a>

                    <a href="photos/park-3.jpg" class="thumbnail" data-categories="park" title="Park Photo 3 Caption">
                        <img src="photos/park-3.jpg" alt="Park 3">
                    </a>

                    <a href="photos/park-4.jpg" class="thumbnail" data-categories="park" title="Park Photo 4 Caption">
                        <img src="photos/park-4.jpg" alt="Park 4">
                    </a>

                    <a href="photos/park-5.jpg" class="thumbnail" data-categories="park" title="Park Photo 5 Caption">
                        <img src="photos/park-5.jpg" alt="Park 5">
                    </a>

                    <a href="photos/park-6.jpg" class="thumbnail" data-categories="park" title="Park Photo 6 Caption">
                        <img src="photos/park-6.jpg" alt="Park 6">
                    </a>

                    <a href="photos/park-7.jpg" class="thumbnail" data-categories="park" title="Park Photo 7 Caption">
                        <img src="photos/park-7.jpg" alt="Park 7">
                    </a>

                    <a href="photos/park-8.jpg" class="thumbnail" data-categories="park" title="Park Photo 8 Caption">
                        <img src="photos/park-8.jpg" alt="Park 8">
                    </a>

                    <a href="photos/park-9.jpg" class="thumbnail" data-categories="park" title="Park Photo 9 Caption">
                        <img src="photos/park-9.jpg" alt="Park 9">
                    </a>

                    <a href="photos/wired-1.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 1 Caption">
                        <img src="photos/wired-1.jpg" alt="Wired 1">
                    </a>

                    <a href="photos/wired-2.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 2 Caption">
                        <img src="photos/wired-2.jpg" alt="Wired 2">
                    </a>

                    <a href="photos/wired-3.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 3 Caption">
                        <img src="photos/wired-3.jpg" alt="Wired 3">
                    </a>

                    <a href="photos/wired-4.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 4 Caption">
                        <img src="photos/wired-4.jpg" alt="Wired 4">
                    </a>

                    <a href="photos/wired-5.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 5 Caption">
                        <img src="photos/wired-5.jpg" alt="Wired 5">
                    </a>

                    <a href="photos/wired-6.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 6 Caption">
                        <img src="photos/wired-6.jpg" alt="Wired 6">
                    </a>

                    <a href="photos/wired-7.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 7 Caption">
                        <img src="photos/wired-7.jpg" alt="Wired 7">
                    </a>

                    <a href="photos/wired-8.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 8 Caption">
                        <img src="photos/wired-8.jpg" alt="Wired 8">
                    </a>

                    <a href="photos/wired-9.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 9 Caption">
                        <img src="photos/wired-9.jpg" alt="Wired 9">
                    </a>

                    <a href="photos/wired-10.jpg" class="thumbnail" data-categories="wired" title="Wired Photo 10 Caption">
                        <img src="photos/wired-10.jpg" alt="Wired 10">
                    </a>

                </div><!-- .thumbnail_wrap end -->

            </div><!-- .photos end -->

        </div><!-- .gallery end -->

    </div><!-- .container end -->
            </section>
           <!--  <section class='client-profile small-container'>
                <img class='client-logo' src='assets/images/logo.png'/>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                <p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</p>
            </section>
            <section class='project-snapshot'>
                <div id="project-snapshot" class="owl-carousel">
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-1b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-2b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-3b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-4b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-1b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-2b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-3b.jpg)'>
                    </div>
                    <div class='project-snapshot-item' style='background-image:url(assets/images/sample-4b.jpg)'>
                    </div>
                </div>
            </section>
            <section class='project-story small-container'>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</p>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.</p>
                <p>Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat.</p>
            </section>-->
            <section class='start-project'>
                <a href='#'>
                    <h1><span>Let's Start Making Your Own Project</span></h1>
                </a>
            </section> 
            <section class='recent-blog'>
                <div class='container'>
                    <h1>Recent Blog Post</h1>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-1b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-2b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-3b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                    <div class='col-4'>
                        <div class='recent-blog-item'>
                            <div class='blog-image' style='background-image:url(assets/images/sample-4b.jpg)'>
                                <a href='#'></a>
                            </div>
                            <h4><a href='#'>Lorem Ipsum Dolor Sit Amet</a></h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci</p>
                        </div>
                    </div>
                </div>
            </section>
            <?php include 'footer.php' ?>
        </main>
    </body>
</html>